/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jpavlich.stemLife;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * @author As You Wish
 */
class GUIController implements ScreenController {
    public static final String SELECTION_DATA = "selectionData";

    private LivingAgent pickedObj;
    private final Nifty gui;

    public GUIController(Nifty gui) {
        this.gui = gui;
    }

    @Override
    public void bind(Nifty nifty, Screen screen) {
    }

    @Override
    public void onStartScreen() {
    }

    @Override
    public void onEndScreen() {
    }

    public void setPickedObject(LivingAgent obj) {
        this.pickedObj = obj;
    }

    private Element getGUIElement(final String id) {
        return gui.getCurrentScreen().findElementByName(id);
    }

    public void update(float tpf) {
        if (pickedObj == null) {
            getGUIElement(SELECTION_DATA).getRenderer(TextRenderer.class).setText("--");
        } else {
            String text = new CellStringBuilder(pickedObj, ToStringStyle.MULTI_LINE_STYLE, null, LivingAgent.class).toString();
            getGUIElement(SELECTION_DATA).getRenderer(TextRenderer.class).setText(text);
        }
    }
}
