/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jpavlich.stemLife;

/**
 *
 * @author As You Wish
 */
public class Probability implements Settable<Double> {
    double probability = 0;
    double probabilityMaxIncrement = 0;
    
    public void increase(float tpf) {
        probability += probabilityMaxIncrement * Math.random() * tpf;
    }
    
    public boolean satisfies() {
        return Math.random() < probability;
    }
    
    public void reset() {
        probability = 0;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        sb.append(probability);
        sb.append(", ");
        sb.append(probabilityMaxIncrement);
        sb.append(")");
        return sb.toString();
    }

    @Override
    public void set(Double value) {
        probabilityMaxIncrement = value;
    }

    @Override
    public Double get() {
        return probabilityMaxIncrement;
    }

}
