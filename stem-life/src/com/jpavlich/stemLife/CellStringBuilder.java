/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jpavlich.stemLife;

import java.lang.reflect.Field;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * @author As You Wish
 */
public class CellStringBuilder extends ReflectionToStringBuilder {
       public <T> CellStringBuilder(
            T object, ToStringStyle style, StringBuffer buffer,
            Class<? super T> reflectUpToClass) {
           super(object, style, buffer, reflectUpToClass, false, false);
       }
       
       @Override
       protected boolean accept(Field field) {
           return field.isAnnotationPresent(Display.class);
       }
}
