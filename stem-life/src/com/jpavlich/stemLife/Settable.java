/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jpavlich.stemLife;

/**
 *
 * @author As You Wish
 */
interface Settable<T> {
    public void set(T value);
    public T get();
}
